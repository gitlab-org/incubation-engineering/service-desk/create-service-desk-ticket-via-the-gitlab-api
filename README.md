# Create Service Desk ticket via the GitLab API

This repository exists to showcase how you can create Service Desk tickets using the GitLab API programmatically.

This will use the `/convert_to_ticket user@example.com` quick action and the issues and notes API.

In order to build an application like this you'll need the following:

1. Your project id (general settings, copy project id)
1. A project level access token with `api` scope and at least the `Reporter` role. (project settings, access tokens)

The example repository uses ruby, but you can use any language. It's quite verbose so you can easily understand the concept.