#!/usr/bin/env ruby

require "uri"
require "net/http"
require "json"

puts 'Create a Service Desk Ticket via API:'

project_id = 'YOUR PROJECT ID'
access_token = 'YOUR PROJECT ACCESS TOKEN'
email = 'email-of-extternal-author@example.com'

query_params = {
  title: 'Service Desk ticket via API',
  description: "This ticket has been created using the issues and notes API.\n/label ~api"
}

issue_uri = URI("https://gitlab.com/api/v4/projects/#{project_id}/issues?#{URI.encode_www_form(query_params)}")

req = Net::HTTP::Post.new(issue_uri)
req['PRIVATE-TOKEN'] = access_token
res = Net::HTTP.start(issue_uri.hostname, issue_uri.port, use_ssl: true) do |http|
  http.request(req)
end

issue_iid = JSON.parse(res.body)['iid']

puts "Created issue with iid #{issue_iid}."

# After we added the issue, we could add additional information to the issue using our crm tools.
# In this example we just add a new note that lists some customer details and adds a sla label

comment_body = "/convert_to_ticket #{email}"

query_params = {
  body: comment_body
}

note_uri = URI("https://gitlab.com/api/v4/projects/#{project_id}/issues/#{issue_iid}/notes?#{URI.encode_www_form(query_params)}")
req = Net::HTTP::Post.new(note_uri)
req['PRIVATE-TOKEN'] = access_token
res = Net::HTTP.start(note_uri.hostname, note_uri.port, use_ssl: true) do |http|
  http.request(req)
end

puts 'Created convert to ticket comment.'

comment_body = 'Hey :wave: thank you for reaching out to us. We\'ll investigate your issue shortly and get back to you as soon as possible.'

query_params = {
  body: comment_body
}

note_uri = URI("https://gitlab.com/api/v4/projects/#{project_id}/issues/#{issue_iid}/notes?#{URI.encode_www_form(query_params)}")
req = Net::HTTP::Post.new(note_uri)
req['PRIVATE-TOKEN'] = access_token
res = Net::HTTP.start(note_uri.hostname, note_uri.port, use_ssl: true) do |http|
  http.request(req)
end

puts 'Created initial "say hello" comment.'
